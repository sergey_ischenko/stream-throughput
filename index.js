// Inspired by
// https://github.com/substack/node-brake/
// plus
// https://nodejs.org/api/readline.html


var r = process.stdin;
r.setEncoding('utf8');

var w = process.stdout;

var rl = require('readline').createInterface({ input: r, output: w });

var brake = require('brake');
var proxy;

// function removeTrainlingNewline(str){
//   if(str && 'string' == typeof(str)){
//     return str.trim();
//   }
// }

function echoInput(speed, cb){
  proxy = brake(speed);

  rl.question('Now enter anything for the echo: ', function(chunk){
    console.log('Got %d bytes of data. Echoing your input (%d bytes/sec): ', chunk.length, speed);
    proxy.pipe(rl);
    proxy.write(chunk + '\n', function(){
      proxy.unpipe(rl);
      cb();
    });
  });
}

function stopOrContinue(){
  rl.question("\nWant to continue (y/n)? ", function(answer){
    if('y' == answer){
      setImmediate(play);
    } else {
      console.log('\nBye!');
      rl.close();
      // process.exit();
      proxy.destroy(); // A more "soft" way to stop the process.
    }
  });
}

function play(){
  rl.question('Enter the speed of the echo (an integer number of bytes/sec): ', function(answer){
    var speed = parseInt(answer);
    if(!speed || Number.isNaN(speed)){
      console.log("Wrong value.\n");
      setImmediate(play);
      return;
    }

    echoInput(speed, stopOrContinue);
  });
}

console.log("*** This program will echo your input to the STDOUT with the speed you specify ***\n");

play();
